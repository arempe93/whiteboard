class TopicsController < ApplicationController

	before_filter :find_course
	before_filter :find_topic, except: [:new, :create, :index]
	before_filter :authenticate_teacher!

	def new
		@topic = Topic.new
	end

	def create
		@topic = @course.topics.build(topic_params)

		if @topic.save
			flash[:success] = "Topic was created"
			redirect_to course_topic_path(@course, @topic)
		else
			flash[:error] = "Error while creating topic"
			render "new"
		end
	end

	def show
	end

	def edit
	end

	def update
		if @topic.update_attributes(topic_params)
			flash[:success] = "Topic was updated"
			redirect_to course_topic_path(@course, @topic)
		else
			flash[:error] = "Error while updating topic"
			render "edit"
		end
	end

	def index
		@topics = @course.topics
	end

	def destroy
		@topic.destroy!

		flash[:info] = "Topic was deleted"
		redirect_to course_topics_path(@course)
	end

	private
		def topic_params
			params.require(:topic).permit(:title, :user_id, :body)
		end

		def find_course
			@course = Course.find(params[:course_id])
		end

		def find_topic
			@topic = Topic.find(params[:id])
		end
end