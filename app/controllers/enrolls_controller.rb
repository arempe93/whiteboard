class EnrollsController < ApplicationController

	before_filter :authenticate_student!

	def new
		@enroll = Enroll.new
	end

	def index
		@course = Course.find(params[:course_id])
		@students = @course.students
	end

	def create
		@course = Course.find(params[:course_id])

		current_student.enroll!(@course)
		flash[:success] = "You are now enrolled in " + @course.title + "."

		redirect_to course_path(@course)
	end

	def destroy
		@enroll = Enroll.find(params[:id])
		@student = @enroll.student
		@course = @enroll.course

		@student.unenroll!(@course)
		flash[:info] = "You are no longer enrolled in " + @course.title + "."

		redirect_to course_path(@course)
	end
end