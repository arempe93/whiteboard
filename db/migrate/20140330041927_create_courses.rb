class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.integer :teacher_id
      t.string :department
      t.integer :number
      t.integer :section
      t.string :title
      t.string :term
      t.string :start_time
      t.string :end_time
      t.text :description
      t.timestamps
    end
  end
end