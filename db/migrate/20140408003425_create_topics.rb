class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.integer :user_id
      t.integer :course_id
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
