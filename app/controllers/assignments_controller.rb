class AssignmentsController < ApplicationController

	before_filter :find_course
	before_filter :find_assignment, except: [:new, :create, :index]

	def new
		@assignment = Assignment.new
	end

	def create

		@assignment = @course.assignments.build(assignment_params)

		if @assignment.save
			flash[:success] = "Assignment was created"
			redirect_to course_assignment_path(@course, @assignment)
		else
			flash[:error] = "Error while creating assignment"
			render "new"
		end
	end

	def show
	end

	def edit
	end

	def update
		if @assignment.update_attributes(assignment_params)
			flash[:success] = "Assignment was updated"
			redirect_to course_assignment_path(@course, @assignment)
		else
			flash[:error] = "Error while updating assignment"
			render "edit"
		end
	end

	def index
		@assignments = @course.assignments
	end

	def destroy
		@assignment.destroy!

		flash[:info] = "Assignment was deleted"
		redirect_to course_assignments_path(@course)
	end

	private
		def assignment_params
			params.require(:assignment).permit(:course_id, :title, :due, :points, :description)
		end

		def find_course
			@course = Course.find(params[:course_id])
		end

		def find_assignment
			@assignment = Assignment.find(params[:id])
		end
end