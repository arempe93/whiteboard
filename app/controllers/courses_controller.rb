class CoursesController < ApplicationController

	before_filter :find_course, except: [:new, :create, :index]

	def new
		@course = Course.new
	end

	def create
		@course = Course.new(course_params)

		if @course.save
			flash[:success] = "Course was created"
			redirect_to course_path(@course)
		else
			flash[:error] = "Error while creating course"
			render "new"
		end
	end

	def show
	end

	def edit
	end

	def update
		if @course.update_attributes(course_params)
			flash[:success] = "Course was updated"
			redirect_to course_path(@course)
		else
			flash[:error] = "Error while updating course"
			render "edit"
		end
	end

	def index
		@courses = Course.all
	end

	def destroy
		@course.destroy!

		flash[:info] = "Course was deleted"
		redirect_to courses_path
	end

	private
		def course_params
			params.require(:course).permit(:teacher_id, :department, :number, :section, :title, :term, :start_time, :end_time, :description)
		end

		def find_course
			@course = Course.find(params[:id])
		end
end