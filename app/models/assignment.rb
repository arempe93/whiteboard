# == Schema Information
#
# Table name: assignments
#
#  id          :integer          not null, primary key
#  course_id   :integer
#  title       :string(255)
#  due         :datetime
#  points      :integer
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Assignment < ActiveRecord::Base

	# Callbacks

	# Validations

	# Relationships
	belongs_to :course
	has_many :grades

	# Functions
end