class EventsController < ApplicationController

	before_filter :find_course
	before_filter :find_event, except: [:new, :create, :index]

	def new
		@event = Event.new
	end

	def create

		@event = @course.events.build(event_params)

		if @event.save
			flash[:success] = "Event was created"
			redirect_to course_event_path(@course, @event)
		else
			flash[:error] = "Error while creating event"
			render "new"
		end
	end

	def show
	end

	def edit
	end

	def update
		if @event.update_attributes(event_params)
			flash[:success] = "Event was updated"
			redirect_to course_event_path(@course, @event)
		else
			flash[:error] = "Error while updating event"
			render "edit"
		end
	end

	def index
		@events = @course.events
		@month = params[:month].to_i
		@week = params[:week]
		@day = params[:day]
		@year = params[:year].to_i
	end

	def destroy
		@event.destroy!

		flash[:info] = "Event was deleted"
		redirect_to course_events_path(@course)
	end

	private
		def event_params
			params.require(:event).permit(:course_id, :title, :description, :event_date, :category)
		end

		def find_course
			@course = Course.find(params[:course_id])
		end

		def find_event
			@event = Event.find(params[:id])
		end
end