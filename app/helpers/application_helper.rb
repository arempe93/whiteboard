module ApplicationHelper

	## Global

	## Teachers

	def course_list(a_teacher)
		list = Array.new

		a_teacher.courses.each do |c|
			list << ["#{course_id(c)} #{c.title}", c.id]
		end

		list
	end

	## Courses

	def student_list(a_course)
		list = Array.new

		a_course.students.each do |s|
			list << [s.name, s.id]
		end

		list
	end

	def assignment_list(a_course)
		list = Array.new

		a_course.assignments.each do |a|
			list << [a.title, a.id]
		end

		list
	end

	def event_list(a_course)
		list = Array.new

		a_course.events.each do |e|
			list << [e.title, e.id]
		end

		list 
	end

	def course_id(a_course)
		"#{a_course.department}#{a_course.number}-#{course_section(a_course)}"
	end

	def course_section(a_course)
		if a_course.section < 10
			return "0#{a_course.section}"
		end
		a_course.section
	end

	## Events

	def category_list
		[
			["Quiz", "Quiz"],
			["Test", "Test"],
			["Exam", "Exam"],
			["Workshop", "Workshop"],
			["In-class Assignment", "In-class Assignment"],
			["Class Project", "Class Project"],
			["Guest Speaker", "Guest Speaker"],
			["Other", "Other"]
		]
	end

	def month_increment(a_month)
		month = a_month + 1

		if month == 13
			return 1
		end
		month
	end

	def month_decrement(a_month)
		month = a_month - 1

		if month == 0
			return 12
		end
		month
	end

	def year_increment(a_month, a_year)
		if a_month == 12
			return a_year + 1
		end
		a_year
	end

	def year_decrement(a_month, a_year)
		if a_month == 1
			return a_year - 1
		end
		a_year
	end

	def weeks_in_month(a_month, a_year)
		first_week_num = Date.new(a_year, a_month, 1).at_beginning_of_week.cweek
		last_week_num = Date.new(a_year, a_month, 1).end_of_month.at_beginning_of_week.cweek
		(first_week_num..last_week_num).to_a
	end

	def calendar_day(date, month)
		if date.month != month
			return ""
		end
		return date.day
	end
end