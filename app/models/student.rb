# == Schema Information
#
# Table name: students
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)      not null
#  last_name              :string(255)      not null
#  student_id             :string(255)      not null
#  grad_year              :integer          not null
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#

class Student < ActiveRecord::Base

	# Devise
	devise :database_authenticatable, :registerable,
		:recoverable, :rememberable, :trackable, :validatable

	# Callbacks

	# Validations
	validates_uniqueness_of :email, :case_sensitive => false
	validates_uniqueness_of :student_id, :case_sensitive => false
	validates :first_name, format: { with: /\A[a-zA-Z]+\z/, message: "does not allow numbers or symbols" }
	validates :last_name, format: { with: /\A[a-zA-Z]+\z/, message: "does not allow numbers or symbols" }

	# Relationships
	has_many :enrolls
	has_many :courses, through: :enrolls

	has_many :grades

	# Functions
	def name
		"#{first_name} #{last_name}"
	end

	def enroll!(a_course)
		enrolls.create!(course_id: a_course.id)
	end

	def unenroll!(a_course)
		enrolls.find_by(course_id: a_course.id).destroy!
	end

	def enrolled?(a_course)
		enrolls.find_by(course_id: a_course.id)
	end
end
