class GradesController < ApplicationController

	before_filter :authenticate_teacher!
	before_filter :find_course
	before_filter :find_grade, except: [:new, :create, :index]

	def new
		@grade = Grade.new
	end

	def create
		@grade = Grade.new(grade_params)

		if @grade.save
			flash[:success] = "Grade was created"
			redirect_to course_grade_path(@course, @grade)
		else
			flash[:error] = "Error while creating grade"
			render "new"
		end
	end

	def show
	end

	def edit
	end

	def update
		if @grade.update_attributes(grade_params)
			flash[:success] = "Grade was updated"
			redirect_to course_grade_path(@course, @grade)
		else
			flash[:error] = "Error while updating grade"
			render "edit"
		end
	end

	def index
		@assignments = @course.assignments
		@grades = Grade.all

		@sorting = params[:sort]
	end

	def destroy
		@grade.destroy!

		flash[:info] = "Grade was deleted"
		redirect_to grades_path
	end

	private
		def grade_params
			params.require(:grade).permit(:student_id, :assignment_id, :points)
		end

		def find_course
			@course = Course.find(params[:course_id])
		end

		def find_grade
			@grade = Grade.find(params[:id])
		end
end