# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  course_id  :integer
#  title      :string(255)
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#

class Topic < ActiveRecord::Base

	# Callbacks

	# Validations

	# Relationships
	belongs_to :course
	belongs_to :user

	# Functions

end
