# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  teacher_id  :integer
#  department  :string(255)
#  number      :integer
#  section     :integer
#  title       :string(255)
#  term        :string(255)
#  start_time  :string(255)
#  end_time    :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Course < ActiveRecord::Base

	# Callbacks

	# Validations

	# Relationships
	belongs_to :teacher

	has_many :enrolls
	has_many :students, through: :enrolls

	has_many :assignments

	has_many :events

	has_many :topics

	# Functions

end