class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception

	def configure_devise_params
		devise_parameter_sanitizer.for(:sign_up) do |u|
			u.permit(:first_name, :last_name, :department, :email, :password, :password_confirmation)
		end
	end
end
