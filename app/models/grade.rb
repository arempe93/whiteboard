# == Schema Information
#
# Table name: grades
#
#  id            :integer          not null, primary key
#  student_id    :integer
#  assignment_id :integer
#  points        :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Grade < ActiveRecord::Base

	# Callbacks

	# Validations

	# Relationships
	belongs_to :assignment
	belongs_to :student

	# Functions
	def percentage
		(points.to_f / assignment.points.to_f * 100).to_i
	end
end