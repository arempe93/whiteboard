# == Schema Information
#
# Table name: enrolls
#
#  id         :integer          not null, primary key
#  course_id  :integer
#  student_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class Enroll < ActiveRecord::Base

	# Callbacks

	# Validations

	# Relationships
	belongs_to :student
	belongs_to :course

	# Functions

end