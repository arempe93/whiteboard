Whiteboard::Application.routes.draw do

	devise_for :students
	devise_for :teachers
	
	resources :students, only: [:show]

	resources :teachers, only: [:show]

	resources :courses do
		resources :assignments
		resources :grades
		resources :topics

		resources :events, except: [:index]
		get '/calendar' => 'events#index'

		resources :enrolls, only: [:create, :index, :destroy]
	end

	root to: 'pages#home'
end