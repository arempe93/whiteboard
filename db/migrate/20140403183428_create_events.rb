class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :category
      t.datetime :event_date
      t.timestamps
    end
  end
end
