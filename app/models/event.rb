# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  course_id   :integer
#  title       :string(255)
#  description :text
#  category    :string(255)
#  event_date  :datetime
#  created_at  :datetime
#  updated_at  :datetime
#

class Event < ActiveRecord::Base

	# Callbacks

	# Validations

	# Relationships
	belongs_to :course

	# Functions
end
